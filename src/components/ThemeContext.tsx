import React, { createContext, useContext, useState } from "react";

interface InitDefault {
  typeTheme: string;
  actionToggle: () => void;
}
const ThemeContext = createContext<InitDefault | null>(null);

function ThemeProvider(props: { children: object }) {
  const [theme, setTheme] = useState("dark");

  const handleToggle = () => {
    setTheme(theme === "dark" ? "light" : "dark");
  };

  const context = {
    typeTheme: theme,
    actionToggle: handleToggle,
  };

  return (
    <ThemeContext.Provider value={context}>
      {props.children}
    </ThemeContext.Provider>
  );
}

export { ThemeContext, ThemeProvider };
