import React, { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Child from "./Child";
import { ThemeContext } from "./ThemeContext";
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";

function Parent() {
  return (
    <>
      <ThemeContext.Consumer>
        {(value) => (
          <button onClick={value?.actionToggle} className="btn">
            {value?.typeTheme === "dark" ? (
              <span>
                <FontAwesomeIcon icon={faSun} /> Light
              </span>
            ) : (
              <span>
                <FontAwesomeIcon icon={faMoon} /> Dark
              </span>
            )}
          </button>
        )}
      </ThemeContext.Consumer>
      <Child />
    </>
  );
}

export default Parent;
