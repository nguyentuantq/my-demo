import React, { useContext } from "react";
import { ThemeContext } from "./ThemeContext";

function Child() {
  const context = useContext(ThemeContext);
  return (
    <ThemeContext.Consumer>
      {(value) => (
        <div className={value?.typeTheme + " theme"}>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Exercitationem, sapiente natus sed recusandae nihil nesciunt
            provident quam hic, quia similique earum voluptate, totam
            praesentium? Excepturi consequatur eum iure aut perferendis!
          </p>
        </div>
      )}
    </ThemeContext.Consumer>
  );
}

export default Child;
